<?php
# statdard header
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);

require_once 'meekrodb.2.2.class.php';
DB::$user = 'mysql';
DB::$password = '';
DB::$dbName = 'test';
DB::$host = 'localhost'; //defaults to localhost if omitted
#DB::$port = '12345'; // defaults to 3306 if omitted
#DB::$encoding = 'utf8'; // defaults to latin1 if omitted




/*
 * <option value="Aronia-A" selected="selected">Aronia-A</option>
 */

function display_block_options($file, $is_dummy_displayed = 0)
{
	$results = parse_ini_file($file);
	$is_first = true;

	//echo "<table>";
	if ($is_dummy_displayed) {
			$is_first = false;
			//echo "<option value=\"\" selected=\"selected\">None</option>";
			echo "<option selected disabled hidden value=''></option>";
	}

	foreach ($results as $row => $val) {
		echo "\n";
		if ($is_first) {
			echo "<option value=\"".$val."\" selected=\"selected\">".$row."</option>";
			$is_first = false;
		} else {
			echo "<option value=\"".$val."\">".$row."</option>";
		}
	}
	//echo "</table>";
}


function display_purpose_options($file, $is_dummy_displayed = 0)
{

	$results = parse_ini_file($file);
	$is_first = true;

	if ($is_dummy_displayed) {
			$is_first = false;
			echo "<option selected disabled hidden value=''></option>";
	}

	foreach ($results as $row => $val) {
		echo "\n";
		if ($is_first) {
			echo "<option value=\"".$val."\" selected=\"selected\">".$row."</option>";
			$is_first = false;
		} else {
			echo "<option value=\"".$val."\">".$row."</option>";
		}
	}
}

function is_exists_pic($vid)
{
	// get length of pic
	$len = DB::queryFirstField("SELECT length(vphoto) FROM visitor WHERE vid=%s", $vid);
	//echo "len is: " . $len . "\n";
	if ($len > 1000) {
		return 1;
	}
	return 0;
}

/**
 * Convert number of seconds into hours, minutes and seconds
 * and return an array containing those values
 *
 * @param integer $seconds Number of seconds to parse
 * @return array
 */
function secondsToTime($seconds)
{
    // extract hours
    $hours = floor($seconds / (60 * 60));
 
    // extract minutes
    $divisor_for_minutes = $seconds % (60 * 60);
    $minutes = floor($divisor_for_minutes / 60);
 
    // extract the remaining seconds
    $divisor_for_seconds = $divisor_for_minutes % 60;
    $seconds = ceil($divisor_for_seconds);
 
    // return the final array
    $obj = array(
        "h" => (int) $hours,
        "m" => (int) $minutes,
        "s" => (int) $seconds,
    );
    return $obj;
}

?>
